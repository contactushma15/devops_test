# # Ubuntu Linux 14.04 LTS
# Python 2.7
# Apache 2.4
# MySQL Ver 14.14 Distrib 5.5

FROM python:2.7-buster

# install apache
RUN apt-get update && apt-get install apache2 mysql-server
COPY apache2.default /etc/apache2/sites-available/default
RUN ln -sf /dev/stdout /var/log/apache/access.log \
    && ln -sf /dev/stderr /var/log/apache/error.log

# copy source and install dependencies
RUN mkdir -p /opt/app
RUN mkdir -p /opt/app/pip_cache
RUN mkdir -p /opt/app/martor_demo
COPY requirements.txt start-server.sh /opt/app/
COPY .pip_cache /opt/app/pip_cache/
COPY martor_demo /opt/app/martor_demo/
WORKDIR /opt/app
CMD ["  cd Multi-Tier-App-Demo/
  sudo bash install.sh
  wget "https://s3.amazonaws.com/richbourg-s3/mtwa/app/ports.conf"
  wget "https://s3.amazonaws.com/richbourg-s3/mtwa/app/000-default.conf"
  sudo cp 000-default.conf /etc/apache2/sites-enabled/
  sudo cp ports.conf /etc/apache2/

"]


# start server
EXPOSE 8000
